import numpy as np
import pandas as pd


'''
## Put nan
dat = pd.read_csv('train.csv')
dat = dat.replace(-1, np.nan)

## Get missing counts
nan_counts = ((dat.isnull().sum() / len(dat.id)) * 100).astype(int)
nan_counts = nan_counts[nan_counts > 30]

## Drop missing features
dat = dat.drop(list(nan_counts.keys()), axis=1)

## Get feature names
features = list(dat.keys())
features.pop(0)
features.pop(0)

## Sort feature types
import functools as fc
def f_prefix(s, l, pfx):
    if s[-l:] == pfx:
        return True
    else:
        return False

cat_features = list(filter(fc.partial(f_prefix, l=3, pfx='cat'), features))
bin_features = list(filter(fc.partial(f_prefix, l=3, pfx='bin'), features))
real_features = [name for name in features if name not in np.append(cat_features, bin_features)]

types = [cat_features,bin_features,real_features]

## Separate catgorical-numeric frames
numeric = dat[real_features]
categorical = dat[cat_features]

## Correlation test
dat_noid = dat.drop(['id'], axis=1)
corr_matrix = dat_noid.corr()

### Minimal correlation, expensive to drop features ###

## Pick out categorical features with only two values
not_categorical = []

for name,series in categorical.iteritems():
    unq = series.unique()
    unq = unq[~np.isnan(unq)]
    if len(unq) == 2:
        not_categorical.append(name)
        
binary = dat[bin_features + not_categorical]
categorical = categorical.drop(not_categorical, axis=1)

bin_features.extend(not_categorical)
cat_features = [name for name in cat_features if name not in not_categorical]

## Barplot target (claim-no claim)
import seaborn as sbn
sbn.countplot(x="target", data=dat)

## Pipelines

Binary features: fill with mode
Categorical features: fill with mode
Real features: fill with median


from sklearn.pipeline import Pipeline,FeatureUnion
from sklearn.preprocessing import Imputer,StandardScaler,LabelBinarizer
from sklearn.base import BaseEstimator, TransformerMixin

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names].values
    
class Multipipe(BaseEstimator, TransformerMixin):
    def __init__(self, operation):
        self.operation = operation
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return np.column_stack([self.operation.fit_transform(column) for column in X.T])

cat_pipeline = Pipeline([
        ('selector', DataFrameSelector(cat_features)),
        ('imputer', Imputer(strategy='most_frequent')),
        ('label_binarizer', Multipipe(LabelBinarizer())),
    ])
    
bin_pipeline = Pipeline([
        ('selector', DataFrameSelector(bin_features)),
        ('imputer', Imputer(strategy='most_frequent')),
    ])
    
real_pipeline = Pipeline([
        ('selector', DataFrameSelector(real_features)),
        ('imputer', Imputer(strategy='median')),
    ])
    
union = FeatureUnion([
        ('pipe_real', real_pipeline),
        ('pipe_bin', bin_pipeline),
        ('pipe_cat', cat_pipeline),
    ])
    
piped = union.fit_transform(dat)
'''

import preprocess

dat, piped = preprocess.import_kaggle()

## Machine learning
from sklearn.model_selection import train_test_split as trsplit
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

X_train,X_test,y_train,y_test = trsplit(piped, dat.target)

piped_0 = X_train[y_train == 0]
piped_1 = X_train[y_train == 1]

piped_0 = np.column_stack([np.array([0] * piped_0.shape[0]),piped_0])
piped_1 = np.column_stack([np.array([1] * piped_1.shape[0]),piped_1])

exmp = 20000
sample_0 = np.random.choice(piped_0.shape[0], exmp)
sample_1 = np.random.choice(piped_1.shape[1], exmp)

rsample_0 = piped_0[sample_0]
rsample_1 = piped_1[sample_1]

#rtarget = np.concatenate((np.array([[0]] * 5000), np.array([[1]] * 5000)))
rsample = np.concatenate((rsample_0, rsample_1))

#rsample = np.column_stack((rtarget, rsample))

np.random.shuffle(rsample)
y = rsample[:,0]
X = rsample[:,1:]



### From: https://www.kaggle.com/c/ClaimPredictionChallenge/discussion/703#5897 ###

def gini(actual, pred, cmpcol = 0, sortcol = 1):
     assert( actual.shape[0] == pred.shape[0] )
     all = np.asarray(np.c_[ actual, pred, np.arange(actual.shape[0]) ], dtype=np.float)
     all = all[ np.lexsort((all[:,2], -1*all[:,1])) ]
     totalLosses = all[:,0].sum()
     giniSum = all[:,0].cumsum().sum() / totalLosses
 
     giniSum -= (len(actual) + 1) / 2.
     return giniSum / actual.shape[0]
 
def gini_normalized(a, p):
     return gini(a, p) / gini(a, a)

######

estimators = [
        { 'name' : 'logisitic_regression', 'obj' : LogisticRegression(), 'predicted' : None, 'acc' : 0.0, 'gini' : 0.0 },
        { 'name' : 'random_forest', 'obj' : RandomForestClassifier(), 'predicted' : None, 'acc' : 0.0, 'gini' : 0.0 },
        { 'name' : 'decision_tree', 'obj' : DecisionTreeClassifier(), 'predicted' : None, 'acc' : 0.0, 'gini' : 0.0 },
        #{ 'name' : 'svm_classifier', 'obj' : SVC(), 'predicted' : None, 'acc' : 0.0, 'gini' : 0.0 },
    ]

for est in estimators:
    classifier = est['obj']
    classifier.fit(X, y)
    est['acc'] = classifier.score(X_test, y_test)
    est['predicted'] = classifier.predict_proba(X_test)[:,1]
    est['gini'] = gini_normalized(y_test, est['predicted'])
    false_neg = 0
    for actual, predicted in zip(y_test, est['predicted']):
        if actual == 1 and predicted <= 0.7:
            false_neg += 1
            
    print(est['name'], ': acc:', est['acc'], ', gini:', est['gini'], 'false_neg:', false_neg, 'actual_pos:', np.sum(y_test == 1))
    
## Logregression Gridsearch
from sklearn.model_selection import GridSearchCV

param_set = { 'C' : [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.7, 0.9, 1.0] }
logreg = LogisticRegression()
clf = GridSearchCV(logreg, param_set)
clf.fit(X, y)

best_attempt = clf.best_estimator_