import numpy as np
import pandas as pd

def import_kaggle():
    ## Put nan
    dat = pd.read_csv('train.csv')
    dat = dat.replace(-1, np.nan)
    
    ## Get missing counts
    nan_counts = ((dat.isnull().sum() / len(dat.id)) * 100).astype(int)
    nan_counts = nan_counts[nan_counts > 30]
    
    ## Drop missing features
    dat = dat.drop(list(nan_counts.keys()), axis=1)
    
    ## Get feature names
    features = list(dat.keys())
    features.pop(0)
    features.pop(0)
    
    ## Sort feature types
    import functools as fc
    def f_prefix(s, l, pfx):
        if s[-l:] == pfx:
            return True
        else:
            return False
    
    cat_features = list(filter(fc.partial(f_prefix, l=3, pfx='cat'), features))
    bin_features = list(filter(fc.partial(f_prefix, l=3, pfx='bin'), features))
    real_features = [name for name in features if name not in np.append(cat_features, bin_features)]
    
    types = [cat_features,bin_features,real_features]
    
    ## Separate catgorical-numeric frames
    numeric = dat[real_features]
    categorical = dat[cat_features]
    
    ## Correlation test
    dat_noid = dat.drop(['id'], axis=1)
    corr_matrix = dat_noid.corr()
    
    ### Minimal correlation, expensive to drop features ###
    
    ## Pick out categorical features with only two values
    not_categorical = []
    
    for name,series in categorical.iteritems():
        unq = series.unique()
        unq = unq[~np.isnan(unq)]
        if len(unq) == 2:
            not_categorical.append(name)
            
    binary = dat[bin_features + not_categorical]
    categorical = categorical.drop(not_categorical, axis=1)
    
    bin_features.extend(not_categorical)
    cat_features = [name for name in cat_features if name not in not_categorical]
    
    ## Barplot target (claim-no claim)
    import seaborn as sbn
    sbn.countplot(x="target", data=dat)
    
    ## Pipelines
    '''
    Binary features: fill with mode
    Categorical features: fill with mode
    Real features: fill with median
    '''
    
    from sklearn.pipeline import Pipeline,FeatureUnion
    from sklearn.preprocessing import Imputer,StandardScaler,LabelBinarizer
    from sklearn.base import BaseEstimator, TransformerMixin
    
    class DataFrameSelector(BaseEstimator, TransformerMixin):
        def __init__(self, attribute_names):
            self.attribute_names = attribute_names
        def fit(self, X, y=None):
            return self
        def transform(self, X):
            return X[self.attribute_names].values
        
    class Multipipe(BaseEstimator, TransformerMixin):
        def __init__(self, operation):
            self.operation = operation
        def fit(self, X, y=None):
            return self
        def transform(self, X):
            return np.column_stack([self.operation.fit_transform(column) for column in X.T])
    
    cat_pipeline = Pipeline([
            ('selector', DataFrameSelector(cat_features)),
            ('imputer', Imputer(strategy='most_frequent')),
            ('label_binarizer', Multipipe(LabelBinarizer())),
        ])
        
    bin_pipeline = Pipeline([
            ('selector', DataFrameSelector(bin_features)),
            ('imputer', Imputer(strategy='most_frequent')),
        ])
        
    real_pipeline = Pipeline([
            ('selector', DataFrameSelector(real_features)),
            ('imputer', Imputer(strategy='median')),
        ])
        
    union = FeatureUnion([
            ('pipe_real', real_pipeline),
            ('pipe_bin', bin_pipeline),
            ('pipe_cat', cat_pipeline),
        ])
        
    piped = union.fit_transform(dat)
    
    return dat, piped